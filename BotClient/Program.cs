﻿using System;
using System.IO;
using Telegram.Bot;
namespace BotClient
{
    class Program
    {

        static TelegramBotClient Bot = new TelegramBotClient("BOTFATHERID");
        static void Main(string[] args)
        {
            Console.WriteLine("Initializing Bot!");
            Bot.StartReceiving();
            Bot.OnMessage += Bot_OnMessage;
            Console.ReadLine();
            //Bot.StopReceiving();
        }
        private static void StopBot()
        {
            Console.WriteLine("stoped Bot! - puede cerrar la app");
        }
        private static void Bot_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                //StartsWith("/start("))
                if (e.Message.Text == "/start")
                {                    
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Hola " + e.Message.Chat.Username + " Bienvenido al mundo de los demonios donde podras acceder a todo el contenido" );
                }
                else if (e.Message.Text == "/stop")
                {
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Saludos " + e.Message.Chat.Username);
                    StopBot();
                }
                
                else if (e.Message.Text == "/ChatSend")
                {
                    Bot.SendContactAsync(e.Message.Chat.Id, "+34664382751", "AlbertGH3");                    
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Saludos" + e.Message.Chat.Username);                    
                }
                else if (e.Message.Text == "/internal.closebot")
                {
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "El bot se cerrara en breve ...");
                    StopBot();
                }
            }
        }
    }
}